<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Op_link_categoria extends Module
{



    public function __construct()
    {
        $this->name = 'op_link_categoria';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'OrangePix';
        $this->need_instance = 1;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Custom Link on Products');
        $this->description = $this->l('Custom Link according Category on Products Page');

        $this->confirmUninstall = $this->l('Are you sure uninstall this module');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        include(dirname(__FILE__).'/sql/install.php');
        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayProductAdditionalInfo') &&
            $this->registerHook('displayHome');

    }

    public function uninstall()
    {

        include(dirname(__FILE__).'/sql/uninstall.php');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('form_button_save')) == true) {
            $this->saveNewButtonLink();
        }
        if (((bool)Tools::isSubmit('submit-btn-delete-Button')) == true) {
            $this->deleteButtonLink();
        }
        if (((bool)Tools::isSubmit('submit-btn-save-Button')) == true) {
            $this->updateButtonLink();
        }


        $token = Tools::getAdminTokenLite('AdminModules');
        $action_form = $this->context->link->getAdminLink('AdminModules', false)
          .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&token='.$token;
        $this->context->smarty->assign('action_form', $action_form);
        $this->context->smarty->assign('module_dir', $this->_path);

        $this->context->smarty->assign('button_list', $this->getButtonListHTML());
        $this->context->smarty->assign('categories', Category::getAllCategoriesName());
        $this->context->smarty->assign('form_new_button', $this->renderForm());
        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output;
    }

    /** New Button Link
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function renderForm()
    {
        $categories = Category::getAllCategoriesName();

        $fields_form = array(
          'form' => array(
            'name' => 'tipo_modal_form',
            'legend' => array(
              'title' => $this->l('New Button Link'),
              'icon' => 'icon-link'

            ),
            'input' => array(

              array(
                'type' => 'select',
                'label' => $this->l('Button Category'),
                'name' => 'button-category',
                'options' => array(

                  'query' => $categories,

                  'id' => 'id_category',

                  'name' => 'name',

                ),
              ),
              array(
                'type' => 'text',
                'lang'  => true,
                'label' => $this->l('Button Link'),
                'name' => 'button-link',
              ),
              array(
                'type' => 'text',
                'lang'  => true,
                'label' => $this->l('Button Text'),
                'name' => 'button-text',
              ),
            ),
            'submit' => array(
              'title' => $this->l('Save'),
            )
          ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $this->fields_form = array();

        $helper->identifier = 'tipo_modal_form';
        $helper->submit_action = 'form_button_save';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
          'languages' => $this->context->controller->getLanguages(),
          'id_language' => $this->context->language->id
        );
        return $helper->generateForm(array($fields_form));
    }


    /**
     * Save form data.
     */
    public function saveNewButtonLink()
    {
        $data1['button_category'] = Tools::getValue('button-category');
        $result = Db::getInstance()->insert($this->name,$data1,false,false,Db::INSERT,false);
        $id = Db::getInstance()->Insert_ID();
        $langs = Language::getLanguages(false);
        foreach ($langs as $lang){
            $data2['button_link'] = Tools::getValue('button-link_'.$lang['id_lang']);
            $data2['button_text'] = Tools::getValue('button-text_'.$lang['id_lang']);
            $data2['id_lang'] = $lang['id_lang'];
            $data2['id_link'] = $id;
            $result = Db::getInstance()->insert($this->name.'_lang',$data2,false,false,Db::INSERT,false);
        }

        die($this->getButtonListHTML());

    }

    /**
     * Get Image List from database
     * @return array|false|mysqli_result|PDOStatement|resource|null
     * @throws PrestaShopDatabaseException
     */
    public function getButtonList(){
        $sql = 'SELECT * FROM '.$this->name.' INNER JOIN '.$this->name.'_lang ON '.$this->name.'.id_link = '.$this->name.'_lang.id_link;';
        $result = Db::getInstance()->executeS($sql);
        $buttons = array();
        foreach ($result as $button){
            $buttons[$button['id_link']]['id_link']= $button['id_link'];
            $buttons[$button['id_link']]['button_category']= $button['button_category'];
            $buttons[$button['id_link']][$button['id_lang']]['link']= $button['button_link'];
            $buttons[$button['id_link']][$button['id_lang']]['text']= $button['button_text'];
        }
        return $buttons;
    }

    /**
     * Return image list in HTML template
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws SmartyException
     */
    public function getButtonListHTML(){
        $this->context->smarty->assign('buttons', $this->getButtonList());
        $this->context->smarty->assign('categories', Category::getAllCategoriesName());
        $this->context->smarty->assign('langs', Language::getLanguages(false));
        $buttons_list = $this->context->smarty->fetch($this->local_path.'views/templates/admin/button-list.tpl');

        return $buttons_list;
    }



    /**
     * Delete Image Card
     */
    public function deleteButtonLink(){
        $id = Tools::getValue('id-button');
        $result = Db::getInstance()->delete($this->name.'_lang','id_link='.$id,0,false,false);
        $result = Db::getInstance()->delete($this->name,'id_link='.$id,0,false,false);

        die($this->getButtonListHTML());
    }

    /**
     * Update Image Card
     */
    public function updateButtonLink(){
        $id = Tools::getValue('id-button');

        $data1['button_category'] = Tools::getValue('button-category');
        $result = Db::getInstance()->update($this->name,$data1,'id_link='.$id,0,false,false,false);

        $langs = Language::getLanguages(false);
        foreach ($langs as $lang){
            $data2['button_link'] = Tools::getValue('button-link_'.$lang['id_lang']);
            $data2['button_text'] = Tools::getValue('button-text_'.$lang['id_lang']);
            $result = Db::getInstance()->update($this->name.'_lang',$data2,'id_link='.$id.' AND id_lang='.$lang['id_lang'],0,false,false,false);
        }

        die($this->getButtonListHTML());
    }



    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('configure') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
//        $this->context->controller->addJS($this->_path.'/views/js/front.js');
//        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    public function hookDisplayHome()
    {
        /* Place your code here. */

    }

    public function hookdisplayProductAdditionalInfo($params){
        // Code for FrontEnd Button Link on Product Page Info
        $product = new Product(Tools::getValue('id_product'));
        $categories = Product::getProductCategories($product->id);
        $buttons = $this->getButtonList();
        foreach ($buttons  as $key => $button) {
            if (!in_array($button['button_category'],$categories)){
                unset($buttons[$key]);
            }
        }
        if (count($buttons)>0){
            $this->context->smarty->assign('buttons',$buttons);
            $this->context->smarty->assign('lang',$this->context->language->id);
            return $this->context->smarty->display($this->local_path.'views/templates/hook/button.tpl');
        }

    }
}
