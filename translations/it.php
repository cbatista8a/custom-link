<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{op_link_categoria}prestashop>op_link_categoria_19e3a37a07dbb7e3df6d17ea99b4bbfc'] = 'Link personalizzato sui prodotti';
$_MODULE['<{op_link_categoria}prestashop>op_link_categoria_24aaef3c337e539d511ccd427cda06a0'] = 'Link personalizzato in base alla categoria nella pagina dei prodotti';
$_MODULE['<{op_link_categoria}prestashop>op_link_categoria_04911cf3ac27849e3ed6aefb2e962ad0'] = 'Sei sicuro di disinstallare questo modulo';
$_MODULE['<{op_link_categoria}prestashop>op_link_categoria_eb4def967b3680814bb38ce5d09ee521'] = 'Nuovo pulsante link';
$_MODULE['<{op_link_categoria}prestashop>op_link_categoria_22ad72b0e9f5160b028360376d091d35'] = 'Categoria del pulsante';
$_MODULE['<{op_link_categoria}prestashop>op_link_categoria_c4b4f98bdf14cc79d1cdc449e1cf78df'] = 'Link del pulsante';
$_MODULE['<{op_link_categoria}prestashop>op_link_categoria_c9cc8cce247e49bae79f15173ce97354'] = 'Salva';
$_MODULE['<{op_link_categoria}prestashop>button-list_584abe63c1c6add8257970c1cce92833'] = 'Categoria: ';
$_MODULE['<{op_link_categoria}prestashop>button-list_c4b4f98bdf14cc79d1cdc449e1cf78df'] = 'Link del pulsante';
$_MODULE['<{op_link_categoria}prestashop>button-list_22ad72b0e9f5160b028360376d091d35'] = 'Categoria del pulsante';
$_MODULE['<{op_link_categoria}prestashop>button-list_c9cc8cce247e49bae79f15173ce97354'] = 'Salva';
$_MODULE['<{op_link_categoria}prestashop>button-list_f2a6c498fb90ee345d997f888fce3b18'] = 'Elimina';
$_MODULE['<{op_link_categoria}prestashop>configure_2b6009f5d14120ed98fee405c0f33ab0'] = 'Link personalizzato in base alla categoria nella pagina dei prodotti';
$_MODULE['<{op_link_categoria}prestashop>configure_e3a276de58def3d533b3aedca46a1d0a'] = 'Nuovo pulsante';
$_MODULE['<{op_link_categoria}prestashop>configure_1843312a5ceabb8e675767c73724c617'] = 'Elenco link pulsanti';
$_MODULE['<{op_link_categoria}prestashop>configure_d3d2e617335f08df83599665eef8a418'] = 'Chiudere';
$_MODULE['<{op_link_categoria}prestashop>button_211e01908806f7bfcbcb599ea99ac593'] = 'I nostri suggerimenti:';
$_MODULE['<{op_link_categoria}prestashop>button_f772c6ea8a95c6a1f625275217759b31'] = 'Scopri i nostri consigli';
