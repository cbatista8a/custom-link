{*
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<!------ Include the above in your HEAD tag ---------->

{* This tab show an preview *}
<div class="panel">
	<h3><i class="icon icon-external-link"></i> {l s='Custom Link Category on Products Page' mod='op_link_categoria'}</h3>
	<div class="col-12">
		<button class="btn btn-primary col-1 pull-right" data-toggle="modal" data-target="#squarespaceModal">{l s='New Button' mod='op_link_categoria'}</button>
	</div>
	<div class="mt-5">
		<h3><i class="icon icon-link"></i> {l s='Button Link List' mod='op_link_categoria'}</h3>
		<div id="list-button" class="mt-5">
			{$button_list}
		</div>
	</div>

</div>

<!-- line modal -->
<div class="modal fade" id="squarespaceModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close pr-1" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">{l s='Close' mod='op_link_categoria'}</span></button>
			</div>
			<div class="modal-body">
				<!-- content goes here -->
				{$form_new_button}
			</div>

		</div>
	</div>
</div>
<!--End line modal -->


