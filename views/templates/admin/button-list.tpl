    {foreach from=$categories item=category key=index}
        {$count=0}
        <ul class="list-unstyled m-3 button-list">
        {foreach from=$buttons item=button key=key}

            {if $category.id_category == $button.button_category}
                {if $count == 0}<span>{l s='Category : ' mod='op_link_categoria'}{$category.name}</span>{$count=$count+1}{/if}
                <li class="media border single-button" data-id="{$button.id_link}">

                    <form class="form-save-button col row" method="post" action="{$action_form}" enctype="multipart/form-data">

                        <div class="media-body col m-3 form-group">

                            <input type="hidden" name="id-button" value="{$button.id_link}">

                            <div class="input-group mb-3 col-4">
                                <div class="form-group">
                                    <label class="" id="button-link-label">{l s='Button Link' mod='op_link_categoria'}</label>
                                    {$count2=0}
                                    {foreach from=$langs item=lang}
                                        <div class="translatable-field lang-{$lang.id_lang}" {if $count2!=0}style="display:none"{/if}{$count2=$count2+1}>
                                            <div class="col-lg-5">
                                                <input type="url" id="button-link_{$lang.id_lang}" name="button-link_{$lang.id_lang}" class="col" value="{$button[$lang.id_lang].link}" onkeyup="if (isArrowKey(event)) return ;updateFriendlyURL();">
                                            </div>
                                            <div class="col-lg-1">
                                                <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown" aria-expanded="false">
                                                    {$lang.iso_code}
                                                    <i class="icon-caret-down"></i>
                                                </button>
                                                <ul class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(5px, 31px, 0px); top: 0px; left: 0px; will-change: transform; overflow: visible" >
                                                    {foreach from=$langs item=lang}
                                                        <li><a href="javascript:hideOtherLanguage({$lang.id_lang});" tabindex="-1">{$lang.name}</a></li>
                                                    {/foreach}
                                                </ul>
                                            </div>
                                        </div>
                                    {/foreach}
                                </div>
                            </div>
                            <div class="input-group mb-3 col-4">
                                <div class="form-group">
                                    <label class="" id="button-link-label">{l s='Button Text' mod='op_link_categoria'}</label>
                                    {$count2=0}
                                    {foreach from=$langs item=lang}
                                        <div class="translatable-field lang-{$lang.id_lang}" {if $count2!=0}style="display:none"{/if}{$count2=$count2+1}>
                                            <div class="col-lg-5">
                                                <input type="url" id="button-text_{$lang.id_lang}" name="button-text_{$lang.id_lang}" class="col" value="{$button[$lang.id_lang].text}" onkeyup="if (isArrowKey(event)) return ;updateFriendlyURL();">
                                            </div>
                                            <div class="col-lg-1">
                                                <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown" aria-expanded="false">
                                                    {$lang.iso_code}
                                                    <i class="icon-caret-down"></i>
                                                </button>
                                                <ul class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(5px, 31px, 0px); top: 0px; left: 0px; will-change: transform; overflow: visible" >
                                                    {foreach from=$langs item=lang}
                                                        <li><a href="javascript:hideOtherLanguage({$lang.id_lang});" tabindex="-1">{$lang.name}</a></li>
                                                    {/foreach}
                                                </ul>
                                            </div>
                                        </div>
                                    {/foreach}
                                </div>
                            </div>
                            <div class="input-group mb-3 col-3">
                                <label class="" for="button-category-{$button.id_link}">{l s='Button Category' mod='op_link_categoria'}</label>
                                <div class="form-group">
                                    <select class="form-control" id="button-category-{$button.id_link}" name="button-category"  style="width: auto" required>
                                        {foreach from=$categories item=category key=key}
                                            <option value="{$category.id_category}" {if $category.id_category == $button.button_category}selected{/if}>{$category.name}</option>
                                        {/foreach}
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="m-3 col-1 form-group" style="height: auto;">
                            <button type="button" class="btn btn-outline-success btn-block border btn-save-button" name="btn-save-button">{l s='Save' mod='op_link_categoria'}</button>
                            <button type="button" class="btn btn-outline-danger btn-block border btn-delete-button" name="btn-delete-button">{l s='Delete' mod='op_link_categoria'}</button>
                        </div>
                    </form>
                </li>
            {/if}
        {/foreach}
        </ul>
    {/foreach}