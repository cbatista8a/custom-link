<!-- .container -->

<div id="custom_link">
    <span>{l s='Our suggestions:' mod='op_link_categoria'}</span><br/>
    {foreach from=$buttons item=button}
    <a class="btn btn-primary m-1" href="{$button[$lang].link}"  role="button" style="margin: 5px;">{if {$button[$lang].text == null || {$button[$lang].text == ''}}}{l s='Discover our advices' mod='op_link_categoria'}{/if}{$button[$lang].text}</a><br/>
    {/foreach}
</div>

