/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/
window.onload= function(){
  
  //On new Button submit
  $('#module_form').on('submit',function (e) {
    e.preventDefault();
    NewButton(this);
  });

  //On values update
  $('.btn-save-button').on('click',function () {
    updateButton(this.form);
  });

  //On Button delete
  $('.btn-delete-button').on('click',function () {
    deleteButton(this.form);
  })



};


/**
 * Send video file to server
 * @param form
 */
function NewButton(form) {
  let data = new FormData(form);
  $.ajax({
    method: 'POST',
    url: form.action,
    data: data,
    processData: false,
    contentType: false,
    cache: false,
    success: function (response) {
      form.reset();
      refreshButtonList(response);
    },
    error: function (response) {

    }
  }).done(function() {

  });
}


/**
 * Update Card values
 * @param form
 */
function updateButton(form) {
  let data = new FormData(form);
  data.append('submit-btn-save-Button','1');
  $.ajax({
    method: 'POST',
    url: form.action,
    data: data,
    processData: false,
    contentType: false,
    cache: false,
    success: function (response) {
      refreshButtonList(response);

    },
    error: function (response) {

    }
  }).done(function() {

  });
}

/**
 * Delete Card Button from banner carousel
 * @param form
 */
function deleteButton(form) {
  let data = new FormData(form);
  data.append('submit-btn-delete-Button','1');
  $.ajax({
    method: 'POST',
    url: form.action,
    data: data,
    processData: false,
    contentType: false,
    cache: false,
    success: function (response) {
      refreshButtonList(response);
    },
    error: function (response) {

    }
  }).done(function() {

  });
}

/**
 * Refresh Button Card List and bind events
 * @param html
 */
function refreshButtonList(html) {
  document.getElementById('list-button').innerHTML = html;
  $('.btn-save-button').on('click',function () {
    updateButton(this.form);
  });

  $('.btn-delete-button').on('click',function () {
    deleteButton(this.form);
  });
}